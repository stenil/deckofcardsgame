package edu.ntnu.idatt2001.stenil.cardgame;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class App extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        System.out.println(getClass().getResource("Scene1.fxml"));
        Parent root = FXMLLoader.load(App.class.getResource("Scene1.fxml"));
        primaryStage.setTitle("Card Game");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }

}
