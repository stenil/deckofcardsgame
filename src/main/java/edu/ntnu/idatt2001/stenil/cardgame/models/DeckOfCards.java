package edu.ntnu.idatt2001.stenil.cardgame.models;

import java.util.ArrayList;
import java.util.Random;
import java.util.*;

public class DeckOfCards {

    private Random random;
    private ArrayList<PlayingCard> playingCards;
    private final char[] suits = {'S', 'H', 'D', 'C'}; //'S'=Spade, 'H'=Heart, 'D'=Diamonds, 'C'=Clubs


    /**
     * Constructor for creating an instance of a deck of cards
     * @throws IllegalArgumentException for illegal sizes of the deck of cards
     */
    public DeckOfCards ()
            throws IllegalArgumentException {

        this.random = new Random();
        this.playingCards = new ArrayList<>();
        refillDeck();

        if (playingCards.size() != 52) {
            throw new IllegalArgumentException("The deck of playingCards needs to contain exactly 52 playingCards.");
        }
    }

    public Random getRandom() {
        return random;
    }

    /**
     * Method for refilling the deck for each iteration a new hand of cards is dealt
     */
    public void refillDeck() {
        playingCards.clear();
        for (char suit : suits) {
            for (int face = 1; face < 14; face++) {
                playingCards.add(new PlayingCard(suit, face));
            }
        }
    }

    /**
     * Method for retrieving the cards from the Playing Cards ArrayList.
     * @return Playing cards.
     */
    public ArrayList<PlayingCard> getCards() {
        return playingCards;
    }

    public char[] getSuits() {
        return suits;
    }

    /**
     * Method for dealing a hand of cards from the deck of cards.
     * @param n number of cards to be dealt
     * @return cards dealt from the deck of cards
     */
    public ArrayList<PlayingCard> dealHand(int n) {
        if(n > playingCards.size() || n < 0) {
            throw new IllegalArgumentException("An error occurred. \n" +
                    "You either entered an invalid number or asked for more cards than what the deck of cards contains."
                    + "\n" + "This deck of cards contains: " + playingCards.size() + " cards.");
        }

        ArrayList<PlayingCard> cardsDealt = new ArrayList<>();
        Collections.shuffle(playingCards, random);

        for (int i = 0; i < n; i++) {
            cardsDealt.add(playingCards.get(0));
            playingCards.remove(0);
        }
        return cardsDealt;
    }

}
