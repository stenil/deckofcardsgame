package edu.ntnu.idatt2001.stenil.cardgame.models;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class HandOfCards {

    private final ArrayList<PlayingCard> playingCards;

    /**
     * Constructor for creating an instance of a hand of playing cards
     * @param playingCards
     */
    public HandOfCards(ArrayList<PlayingCard> playingCards) {
        this.playingCards = playingCards;
    }

    /**
     * Method for retrieving playing cards from the ArrayList.
     * @return a String
     */
    public String getPlayingCards() {
        StringBuilder sb = new StringBuilder();
        playingCards.stream().toList().forEach(playingCard -> sb.append(playingCard.getAsString() + " "));
        return sb.toString();
    }

    /**
     * Method for getting the sum of faces from the cards on hand
     * @return the sum of the faces shown on the hand of cards
     */
    public int getSumOfCards() {
        return playingCards.stream().mapToInt(PlayingCard::getFace).sum(); //mapToInt returns the values of the
                                                                           //class::method as an int
    }

    /**
     * Method for checking whether the five cards on hand is a flush,
     * which implies that all cards have identical suits regardless of face
     * @return true if hand is a flush
     */
    public boolean hasFlushOnHand() {
        if (playingCards.size() != 5) {
            return false;
        }
        return playingCards.stream().allMatch((playingCard) -> playingCard.getSuit() == playingCards.get(0).getSuit());
    }

    /**
     * Method for getting all the cards on hand showing the 'Hearts' suit
     * @return only the playing cards with 'Hearts' suit as a String
     */
    public String getHearts() {
        StringBuilder sb = new StringBuilder();

        playingCards.stream().filter(playingCard -> playingCard.getSuit() == 'H').toList()
                .forEach(playingCard -> sb.append(playingCard.getAsString()));

        return sb.toString();
    }

    /**
     * Method to check if a dealt card is a Queen of Spades.
     * @return true if Queen of Spades is on hand
     */
    public boolean cardIsQueenOfSpades() {
        return playingCards.stream().anyMatch((playingCard)
                -> playingCard.getFace() == 12 && playingCard.getSuit() == 'S');
    }
}