package edu.ntnu.idatt2001.stenil.cardgame.controllers;

import edu.ntnu.idatt2001.stenil.cardgame.models.DeckOfCards;
import edu.ntnu.idatt2001.stenil.cardgame.models.HandOfCards;
import edu.ntnu.idatt2001.stenil.cardgame.models.PlayingCard;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

public class Scene1Controller {
    private DeckOfCards deckOfCards = new DeckOfCards();
    private HandOfCards handOfCards;

    @FXML
    public Label cards;
    @FXML
    public TextField sumCardFaces;
    @FXML
    public TextField getHearts;
    @FXML
    public TextField flushCheck;
    @FXML
    public TextField queenOfSpadesCheck;

    public void dealHand(ActionEvent actionEvent) {
        handOfCards = new HandOfCards(deckOfCards.dealHand(5));
        deckOfCards.refillDeck();
        cards.setText(handOfCards.getPlayingCards());
    }

    public void checkHand(ActionEvent actionEvent) {
        sumCardFaces.setText(Integer.toString(handOfCards.getSumOfCards()));
        getHearts.setText(handOfCards.getHearts());
        flushCheck.setText(handOfCards.hasFlushOnHand() ? "Yes" : "No");
        if (handOfCards.cardIsQueenOfSpades()) {
            queenOfSpadesCheck.setText("Yes");
        } else {
            queenOfSpadesCheck.setText("No");
        }

    }
}
