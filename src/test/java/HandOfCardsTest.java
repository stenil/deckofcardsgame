import edu.ntnu.idatt2001.stenil.cardgame.models.HandOfCards;
import edu.ntnu.idatt2001.stenil.cardgame.models.PlayingCard;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class HandOfCardsTest {

    @Test
    public void testSumOfFaces() {
        ArrayList<PlayingCard> testCards = new ArrayList<>();
        HandOfCards testHand = new HandOfCards(testCards);

        testCards.add(new PlayingCard('D', 1));
        testCards.add(new PlayingCard('D', 2));
        testCards.add(new PlayingCard('C', 3));

        assertEquals(6, testHand.getSumOfCards());
    }

    @Test
    public void testThatFlashIsIdentified() {
        ArrayList<PlayingCard> handWithFlush = new ArrayList<>();
        ArrayList<PlayingCard> handWithoutFlush = new ArrayList<>();

        handWithFlush.add(new PlayingCard('S', 12));
        handWithFlush.add(new PlayingCard('S', 11));
        handWithFlush.add(new PlayingCard('S', 10));
        handWithFlush.add(new PlayingCard('S', 9));
        handWithFlush.add(new PlayingCard('S', 8));

        handWithoutFlush.add(new PlayingCard('D', 13));
        handWithoutFlush.add(new PlayingCard('C', 11));
        handWithoutFlush.add(new PlayingCard('C', 10));
        handWithoutFlush.add(new PlayingCard('H', 9));
        handWithoutFlush.add(new PlayingCard('H', 8));


        HandOfCards testHandWithFlush = new HandOfCards(handWithFlush);
        HandOfCards testHandWithoutFlush = new HandOfCards(handWithoutFlush);

        assertTrue(testHandWithFlush.hasFlushOnHand());
        assertFalse(testHandWithoutFlush.hasFlushOnHand());
    }

    @Test
    public void testThatQueenOfSpadesIsIdentified() {
        ArrayList<PlayingCard> handWithQueen = new ArrayList<>();
        ArrayList<PlayingCard> handWithoutQueen = new ArrayList<>();

        handWithQueen.add(new PlayingCard('S', 12));
        handWithQueen.add(new PlayingCard('H', 11));
        handWithQueen.add(new PlayingCard('D', 10));
        handWithQueen.add(new PlayingCard('C', 9));
        handWithQueen.add(new PlayingCard('C', 8));

        handWithoutQueen.add(new PlayingCard('D', 13));
        handWithoutQueen.add(new PlayingCard('C', 1));
        handWithoutQueen.add(new PlayingCard('C', 10));
        handWithoutQueen.add(new PlayingCard('H', 9));
        handWithoutQueen.add(new PlayingCard('S', 10));


        HandOfCards handWithQueenOfSpades = new HandOfCards(handWithQueen);
        HandOfCards handWithNoQueenOfSpades = new HandOfCards(handWithoutQueen);

        assertTrue(handWithQueenOfSpades.cardIsQueenOfSpades());
        assertFalse(handWithNoQueenOfSpades.cardIsQueenOfSpades());
    }

    @Test
    public void testThatMethodForCheckingCardsWithHeartsWorks() {
        ArrayList<PlayingCard> handWithHearts = new ArrayList<>();
        ArrayList<PlayingCard> handWithoutHearts = new ArrayList<>();

        handWithHearts.add(new PlayingCard('H', 12));
        handWithHearts.add(new PlayingCard('S', 11));
        handWithHearts.add(new PlayingCard('S', 10));
        handWithHearts.add(new PlayingCard('S', 9));
        handWithHearts.add(new PlayingCard('S', 8));

        handWithoutHearts.add(new PlayingCard('D', 13));
        handWithoutHearts.add(new PlayingCard('C', 1));
        handWithoutHearts.add(new PlayingCard('C', 10));
        handWithoutHearts.add(new PlayingCard('C', 9));
        handWithoutHearts.add(new PlayingCard('S', 10));


        HandOfCards aHandWithHearts = new HandOfCards(handWithHearts);
        HandOfCards aHandWithNoHearts = new HandOfCards(handWithoutHearts);

        assertEquals("12 of H\n", aHandWithHearts.getHearts());
        assertNotEquals("13 of D\n", aHandWithNoHearts.getHearts());
    }
}
