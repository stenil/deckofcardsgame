import com.sun.nio.sctp.IllegalReceiveException;
import edu.ntnu.idatt2001.stenil.cardgame.models.DeckOfCards;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class DeckOfCardsTest {

    @Test
    public void testThatRefillingTheDeckWorks() {
        DeckOfCards testDeck = new DeckOfCards();
        testDeck.dealHand(10); //Deals 10 cards
        testDeck.refillDeck();   //Refills the deck of cards
        assertNotEquals(10, testDeck.getCards());
    }

    @Test
    public void testIfEnteringAnInvalidNumberOfCardsToBeDealtThrowsException() {
        DeckOfCards testDeck1 = new DeckOfCards();
        DeckOfCards testDeck2 = new DeckOfCards();

        assertThrows(IllegalArgumentException.class, () -> testDeck1.dealHand(-5));
        assertThrows(IllegalArgumentException.class, () -> testDeck2.dealHand(69));

    }

}
