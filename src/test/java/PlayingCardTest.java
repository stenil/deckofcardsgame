import edu.ntnu.idatt2001.stenil.cardgame.models.PlayingCard;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class PlayingCardTest {

    @Test
    void testThatOnlyValidCardsAreMade() {
        assertThrows(RuntimeException.class, () -> new PlayingCard('A', 7));
        assertThrows(RuntimeException.class, () -> new PlayingCard('S', 14));
        assertDoesNotThrow(() -> new PlayingCard('S', 12));
    }
}
